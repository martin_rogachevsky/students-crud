﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using StudentsXML.Domain;

namespace StudentsXML.Xml
{
    public class StudentsXmlReader
    {
        public StudentSet Read()
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() != DialogResult.OK) return new StudentSet();
            var xmlSerializer = new XmlSerializer(typeof(StudentSet));
            using(var streamReader = new StreamReader(openFileDialog.FileName))
            {
                try
                {
                    return xmlSerializer.Deserialize(streamReader) as StudentSet;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return new StudentSet();
                }
            }
        }
    }
}
