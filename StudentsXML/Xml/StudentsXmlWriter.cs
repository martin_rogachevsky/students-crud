﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using StudentsXML.Domain;

namespace StudentsXML.Xml
{
    public class StudentsXmlWriter
    {
        public void Write(StudentSet list)
        {
            var saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() != DialogResult.OK) return;
            var xmlSerializer = new XmlSerializer(typeof(StudentSet));
            using (var streamWriter = new StreamWriter(saveFileDialog.FileName))
            {
                try
                {
                    xmlSerializer.Serialize(streamWriter, list);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
