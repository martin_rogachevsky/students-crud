﻿using System;
using System.Xml.Serialization;

namespace StudentsXML.Domain
{
    [XmlType("Student")]
    public class Student
    {
        public enum StudentGender
        {
            [XmlEnum("0")]
            Male = 0,

            [XmlEnum("1")]
            Female = 1
        }

        [XmlAttribute]
        public int Id { get; set; }

        public String FirstName { get; set; }

        public String Last { get; set; }

        public int Age { get; set; }

        public StudentGender Gender { get; set; }

        public override string ToString()
        {
            int lastNum = Age % 10;
            String yearPostfix = lastNum == 1 ? "год" : (lastNum > 1 && lastNum < 5 ? "года" : "лет");
            String gender = (Gender == StudentGender.Male) ? "мужской":"женский";
            return $@"Id: {Id}, Имя: {FirstName} {Last}, Возраст: {Age} {yearPostfix}, Пол: {gender}.";
        }
    }
}
