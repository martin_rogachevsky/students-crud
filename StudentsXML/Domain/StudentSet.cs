﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace StudentsXML.Domain
{
    [XmlRoot("Students")]
    [XmlInclude(typeof(Student))]
    public class StudentSet: ObservableCollection<Student>
    {
        public void Remove(IEnumerable<Student> collection)
        {
            if (MessageBox.Show(@"Вы уверены, что хотите удалить элементы?", @"Вы уверены?",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                foreach (var student in collection.ToArray())
                {
                   base.Remove(student);
                }
            }
        }

        public new bool Remove(Student student)
        {
            if (MessageBox.Show(@"Вы уверены, что хотите удалить элемент?", @"Вы уверены?",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                return base.Remove(student);
            }
            return false;
        }
    }
}
