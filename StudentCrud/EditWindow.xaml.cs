﻿using System;
using System.Windows;
using StudentsXML.Domain;

namespace StudentCrud
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        public Student Result { get; set; }

        public EditWindow()
        {
            InitializeComponent();
        }

        public EditWindow(Student student)
        {
            InitializeComponent();
            FirstNameTextBox.Text = student.FirstName;
            LastNameTextBox.Text = student.Last;
            MaleRadioButton.IsChecked = student.Gender == Student.StudentGender.Male;
            AgeTextBox.Text = student.Age.ToString();
        }

        private bool IsDataValid()
        {
            if (String.IsNullOrWhiteSpace(FirstNameTextBox.Text) || String.IsNullOrWhiteSpace(FirstNameTextBox.Text))
            {
                MessageBox.Show("Имя и фамилия обязательны для заполнения!","Внимание!", MessageBoxButton.OK);
                return false;
            }
            if (IsAgeValid())
            {
                int age = Int32.Parse(AgeTextBox.Text);
                if (age < 16 || age > 100)
                {
                    MessageBox.Show("Возраст должен быть в диапазоне [16, 100]!", "Внимание!", MessageBoxButton.OK);
                    return false;
                }
            }
            return true;
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Ok_Button_Click(object sender, RoutedEventArgs e)
        {
            if (IsDataValid())
            {
                Result = new Student()
                {
                    FirstName = FirstNameTextBox.Text,
                    Last = LastNameTextBox.Text,
                    Gender = MaleRadioButton.IsChecked ?? false
                        ? Student.StudentGender.Male
                        : Student.StudentGender.Female,
                    Age = Int32.Parse(AgeTextBox.Text)
                };
                DialogResult = true;
                Close();
            }
        }

        private bool IsAgeValid()
        {
            int t;
            return String.IsNullOrWhiteSpace(AgeTextBox.Text) && int.TryParse(AgeTextBox.Text, out t);
        }

       
    }
}
