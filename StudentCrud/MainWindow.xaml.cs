﻿using System.Linq;
using StudentsXML.Domain;
using StudentsXML.Xml;
using System.Windows;
using System.Windows.Controls;

namespace StudentCrud
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly StudentSet _list;

        public MainWindow()
        {
            InitializeComponent();
            _list = new StudentsXmlReader().Read();
            StudentsBox.ItemsSource = _list;
            _list.CollectionChanged += _list_CollectionChanged;
            StudentsBox.SelectionMode = SelectionMode.Multiple;
            if (_list.Any())
            {
                InfoLabel.Visibility = Visibility.Hidden;
                StudentsBox.Visibility = Visibility.Visible;
            }
        }

        private void _list_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (!_list.Any())
            {
                InfoLabel.Visibility = Visibility.Visible;
                StudentsBox.Visibility = Visibility.Hidden;
            }
            else
            {
                InfoLabel.Visibility = Visibility.Hidden;
                StudentsBox.Visibility = Visibility.Visible;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            new StudentsXmlWriter().Write(_list);
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (StudentsBox.SelectedItems.Count > 1)
                _list.Remove(StudentsBox.SelectedItems.Cast<Student>());
            else
                _list.Remove(StudentsBox.SelectedItems[0] as Student);
        }

        private void StudentsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (StudentsBox.SelectedItems.Count == 1)
            {
                EditButton.IsEnabled = true;
                DeleteButton.IsEnabled = true;
            }
            else if (StudentsBox.SelectedItems.Count > 1)
            {
                EditButton.IsEnabled = false;
                DeleteButton.IsEnabled = true;
            }
            else
            {
                EditButton.IsEnabled = false;
                DeleteButton.IsEnabled = false;
            }
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            var editWindow = new EditWindow();
            if (editWindow.ShowDialog() ?? false)
            {
                _list.Add(editWindow.Result);    
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            var student = StudentsBox.SelectedItems[0] as Student;
            var editWindow = new EditWindow(student);
            if (editWindow.ShowDialog() ?? false)
            {
                _list[_list.IndexOf(student)] = editWindow.Result;
            }
        }
    }
}
